﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Linq;
using System.Reflection;

using System.Collections.Generic;


namespace CsProjComplete
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            FileInfo csprojFileInfo = null;
            DirectoryInfo rootDirectoryInfo = null;

            var inputPath =
                args.Length > 0 ?
                    args[0] :
                    Assembly.GetExecutingAssembly().Location;

            var csprojFileFound = false;
            do
            {
                while (string.IsNullOrEmpty(inputPath))
                {
                    Console.WriteLine("Input csproj file path or directory path:");
                    inputPath = Console.ReadLine();
                }
                try
                {
                    if (File.Exists(inputPath) || Directory.Exists(inputPath))
                    {
                        var fileAttr = File.GetAttributes(inputPath);
                        if ((fileAttr & FileAttributes.Directory) == FileAttributes.Directory)
                        {
                            rootDirectoryInfo = new DirectoryInfo(inputPath);
                            var findRes = FindFile(rootDirectoryInfo, ".csproj");
                            if (findRes.Any())
                            {
                                if (findRes.Count() != 1)
                                {
                                    Console.WriteLine(
                                        "Cannot work with multiple csproj file under same dir."
                                    );
                                    Console.ReadKey();
                                    return;
                                }
                                csprojFileInfo = findRes.First();
                                csprojFileFound = true;
                            }
                        }
                        else if ((fileAttr & FileAttributes.Normal) == FileAttributes.Normal)
                        {
                            var assemblyFileInfo = new FileInfo(inputPath);
                            rootDirectoryInfo = assemblyFileInfo.Directory;
                            var findRes = FindFile(rootDirectoryInfo, ".csproj");
                            if (findRes.Any())
                            {
                                if (findRes.Count() != 1)
                                {
                                    Console.WriteLine(
                                        "Cannot work with multiple csproj file under same dir."
                                    );
                                    Console.ReadKey();
                                    return;
                                }
                                csprojFileInfo = findRes.First();
                                csprojFileFound = true;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine(
                            "No files or directory found with path: {0}",
                            inputPath
                        );
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
                finally
                {
                    if (csprojFileFound == false)
                    {
                        Console.WriteLine("csproj file not found");
                        csprojFileInfo = null;
                        rootDirectoryInfo = null;
                        inputPath = null;
                    }
                }
            }
            while (csprojFileInfo == null || rootDirectoryInfo == null);


            var csFileInfos = FindFileRecursion(rootDirectoryInfo, ".cs", new[] {"bin", "obj"});
            var content = string.Empty;
            try
            {
                using (var reader = csprojFileInfo.OpenText()) {
                    content = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                throw;
            }
            var doc = new XmlDocument();
            doc.LoadXml(content);
            var root = doc.DocumentElement;
            var nodeList = root.GetElementsByTagName("ItemGroup");
            XmlNode csNode = null;
            foreach (XmlNode itemGroupNode in nodeList)
            {
                foreach (XmlNode compileNode in itemGroupNode)
                {
                    if (compileNode.Name != "Compile")
                        continue;
                    csNode = itemGroupNode;
                    break;
                }
                if(csNode != null)
                    break;
            }
            if (csNode == null)
            {
                var element = doc.CreateElement("ItemGroup", doc.DocumentElement.NamespaceURI);
                root.AppendChild(element);
                csNode = element;
            }
            else
            {
                csNode.RemoveAll();
            }
            foreach (var fileInfo in csFileInfos)
            {
                var relativePath = GetRelativePath(rootDirectoryInfo, fileInfo);
                var element = doc.CreateElement("Compile", doc.DocumentElement.NamespaceURI);
                element.SetAttribute("Include", relativePath);
                csNode.AppendChild(element);
            }
            var writer = csprojFileInfo.CreateText();
            writer.Write(Beautify(doc));
            writer.Close();
            Console.Write("Success");
            Console.Read();
        }


        private static IEnumerable<FileInfo> FindFileRecursion(
                DirectoryInfo parentDirectoryInfo,
                string extension,
                string[] exclusive = null)
        {
            var childDirectoryInfos = parentDirectoryInfo.GetDirectories();
            var childFileInfos = parentDirectoryInfo.GetFiles();
            var result = childFileInfos
               .Where(fileInfo => fileInfo.Extension == extension).ToList();

            foreach (var directory in childDirectoryInfos)
            {
                if (exclusive != null && Array.Find(exclusive, (t) => t == directory.Name) != null)
                {
                    continue;
                }
                result.AddRange(FindFileRecursion(directory,extension));
            }
            return result;
        }


        private static List<FileInfo> FindFile(
                DirectoryInfo parentDirectoryInfo,
                string extension)
        {
            return parentDirectoryInfo.GetFiles()
               .Where(fileInfo => fileInfo.Extension == extension).ToList();
        }


        private static string GetRelativePath(
                DirectoryInfo directoryInfo,
                FileInfo fileInfo)
        {
            if (directoryInfo == null || fileInfo == null)
                return null;
            var filePath = fileInfo.FullName;
            var diretoryPath = directoryInfo.FullName;

            var res = filePath.Substring(0, diretoryPath.Length) != diretoryPath ?
                null :
                filePath.Substring(
                    diretoryPath.Length + 1,
                    filePath.Length - diretoryPath.Length - 1
                );
            res = res.Replace('/','\\');  // keep windows path separator
            return res;
        }


        private static string Beautify(XmlDocument doc)
        {
            var sb = new StringBuilder();
            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = System.Environment.NewLine,
                NewLineHandling = NewLineHandling.Replace
            };
            using (var writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            return sb.ToString();
        }
    }
}
